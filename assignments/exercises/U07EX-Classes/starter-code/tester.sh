clear
echo "-------------------------------------------------"
echo "Build program 1..."
g++ program1/*.h program1/*.cpp -o program1exe
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe - input 20, 6"
echo ""
echo "EXPECTED OUTPUT: Rolls a 20 sided die 6 times"
echo ""
echo "ACTUAL OUTPUT:"
echo 20 6 | ./program1exe
echo ""
echo "-------------------------------------------------"
echo "CODE:"
cat program1/Dice.cpp
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 2..."
g++ program2/*.h program2/*.cpp -o program2exe
echo "-------------------------------------------------"
echo "TEST 1: ./program2exe - print"
echo ""
echo "EXPECTED OUTPUT: Correct"
echo ""
echo "ACTUAL OUTPUT:"
echo print | ./program2exe
echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program2exe - input"
echo ""
echo "EXPECTED OUTPUT: Incorrect"
echo ""
echo "ACTUAL OUTPUT:"
echo input | ./program2exe
echo ""
echo "-------------------------------------------------"
echo "CODE:"
echo "Question.h"
cat program2/Question.h
echo "Question.cpp"
cat program2/Question.cpp
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 3..."
g++ program3/*.h program3/*.cpp -o program3exe
echo "-------------------------------------------------"
echo "TEST 1: ./program3exe - a b c d"
echo ""
echo "EXPECTED OUTPUT: All incorrect"
echo ""
echo "ACTUAL OUTPUT:"
echo a b c d | ./program3exe
echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program3exe - pineapple Jupiter 3 C++"
echo ""
echo "EXPECTED OUTPUT: All correct"
echo ""
echo "ACTUAL OUTPUT:"
echo pineapple Jupiter 3 C++ | ./program3exe
echo ""
echo "-------------------------------------------------"
echo "CODE:"
echo "Quiz.h"
cat program3/Question.h
echo "Quiz.cpp"
cat program3/Question.cpp
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 4..."
g++ program4/*.h program4/*.cpp -o program4exe
echo "-------------------------------------------------"
echo "TEST 1: ./program3exe - a b c d"
echo ""
echo "EXPECTED OUTPUT: Displays course name, code, teacher, and students"
echo ""
echo "ACTUAL OUTPUT:"
./program4exe
echo ""
echo "-------------------------------------------------"
echo "CODE:"
echo "Course.h"
cat program4/Course.h
echo "Course.cpp"
cat program4/Course.cpp
echo "Person.h"
cat program4/Person.h
echo "Person.cpp"
cat program4/Person.cpp
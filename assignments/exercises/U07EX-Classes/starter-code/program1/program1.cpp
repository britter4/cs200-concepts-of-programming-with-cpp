#include <cstdlib>
#include <ctime>
#include <iostream>
using namespace std;

int main() {
  srand(time(NULL));

  /* Program ends with no errors (error code 0) */
  cout << endl << "GOODBYE!" << endl;
  return 0;
}
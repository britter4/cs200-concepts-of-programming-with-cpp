#include <iostream> /* Use cout */
#include <string>   /* Use strings */
using namespace std;

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 4) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " name course semester year"
         << endl;
    return 1; /* Exit with error code 1 */
  }

  // Get the first argument and store it in a string variable
  string name = argumentList[1];

  // Get the second argument and store it in a string variable
  string course = argumentList[2];

  // Get the third argument and store it in a string variable
  string semester = argumentList[3];

  // Get the fourth argument and convert it to an integer, store in an integer
  // variable.
  int year = stoi(argumentList[4]);

  // Display the welcome message.
  cout << "Hi " << name << "," << endl;
  cout << "Welcome to " << semester << " semester, " << year << endl;
  cout << "Looking forward to having you in " << course << "!" << endl;

  /* Quit program with code 0 (no errors) */
  return 0;
}
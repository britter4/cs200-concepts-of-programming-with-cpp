#include <iostream> /* Use cout */
#include <string>   /* Use strings */
using namespace std;

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " number1 number2" << endl;
    return 1; /* Exit with error code 1 */
  }

  // Get the first argument, convert it to float, store in a variable
  float number1 = stof(argumentList[1]);

  // Get the second argument, convert it to float, store in a variable
  float number2 = stof(argumentList[2]);

  // Create a third float, add the two numbers and store the result in the
  // variable.
  float sum = number1 + number2;

  // Display the result of the calculation in a human-readable format.
  cout << number1 << " + " << number2 << " = " << sum << endl;

  /* Quit program with code 0 (no errors) */
  return 0;
}
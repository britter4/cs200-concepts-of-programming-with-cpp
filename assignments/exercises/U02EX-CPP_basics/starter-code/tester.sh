clear
echo "-------------------------------------------------"
echo "Build program 1..."
clang++ program1.cpp -o program1exe
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program1exe Rachel CS200 Summer 2022 JCCC"
echo ""
echo "EXPECTED OUTPUT:"
echo "Hi Rachel,"
echo "Welcome to JCCC during the Summer semester, 2022"
echo "Looking forward to having you in CS200!"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe Rachel CS200 Summer 2022 JCCC

echo ""
echo "-------------------------------------------------"
echo "TEST 3: ./program1exe Alexa CS235 Fall 2020 MCCKC"
echo ""
echo "EXPECTED OUTPUT:"
echo "Hi Alexa,"
echo "Welcome to MCCKC during the Fall semester, 2020"
echo "Looking forward to having you in CS235!"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe Alexa CS235 Fall 2020 MCCKC

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 2..."
clang++ program2.cpp -o program2exe
echo "-------------------------------------------------"
echo "TEST 1: ./program2exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program2exe 1.1 2.2 3.3"
echo ""
echo "EXPECTED OUTPUT:"
echo "1.1 + 2.2 + 3.3 = 6.6"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe 1.1 2.2 3.3

echo ""
echo "-------------------------------------------------"
echo "TEST 3: ./program2exe -5 -3 -1"
echo ""
echo "EXPECTED OUTPUT:"
echo "-5 + -3 + -1 = -9"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe -5 -3 -1

echo ""
echo "-------------------------------------------------"
echo "TEST CASES:"
cat program2_tests.txt

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 3..."
clang++ program3.cpp -o program3exe
echo "-------------------------------------------------"
echo "TEST 1: ./program3exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program3exe 6.5 7.0"
echo ""
echo "EXPECTED OUTPUT:"
echo "Room length: 6.5 ft"
echo "Room width: 7.0 ft"
echo "Room perimeter: 27 ft"
echo "Room area: 45.5 sqft"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe 6.5 7.0

echo ""
echo "-------------------------------------------------"
echo "TEST 3: ./program3exe 4 5"
echo ""
echo "EXPECTED OUTPUT:"
echo "Room length: 4 ft"
echo "Room width: 5 ft"
echo "Room perimeter: 18 ft"
echo "Room area: 20 sqft"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe 4 5
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 4..."
clang++ program4.cpp -o program4exe
echo "-------------------------------------------------"
echo "TEST 1: ./program4exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo ""
echo "ACTUAL OUTPUT:"
./program4exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program4exe 2 3"
echo ""
echo "EXPECTED OUTPUT:"
echo "2 adult tickets at \$7.95 per ticket: \$15.90"
echo "3 child tickets at \$5.95 per ticket: \$17.85"
echo "Total price: \$33.75"
echo ""
echo "ACTUAL OUTPUT:"
./program4exe 2 3

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program4exe 1 1"
echo ""
echo "EXPECTED OUTPUT:"
echo "1 adult tickets at \$7.95 per ticket: \$7.95"
echo "1 child tickets at \$5.95 per ticket: \$5.95"
echo "Total price: \$13.90"
echo ""
echo "ACTUAL OUTPUT:"
./program4exe 1 1

echo ""
echo "-------------------------------------------------"
echo "TEST CASES:"
cat program4_tests.txt
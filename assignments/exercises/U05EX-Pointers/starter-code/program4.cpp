#include <fstream>  /* use ifstream and ofstream */
#include <iomanip>  /* Use setprecision */
#include <iostream> /* Use cout */
#include <string>   /* Use strings */
using namespace std;

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " input_file" << endl;
    return 1; /* Exit with error code 1 */
  }

  string filename = argumentList[1];

  int arraySize; // will be loaded from file

  // Add code here

  /* Quit program with code 0 (no errors) */
  return 0;
}
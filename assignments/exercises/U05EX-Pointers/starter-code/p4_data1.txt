12
CS 134   Programming Fundamentals
CS 200   Concepts of Programming Algorithms Using C++
CS 201   Concepts of Programming Algorithms using C#
CS 202   Concepts of Programming Algorithms using Python
CS 205   Concepts of Programming Algorithms using Java
CS 210   Discrete Structures I
CS 211   Discrete Structures II
CS 235   Object-Oriented Programming Using C++
CS 236   Object-Oriented Programming Using C#
CS 250   Basic Data Structures using C++
CS 252   Basic Data Structures Using Python
CS 255   Basic Data Structures Using Java
clear
echo "-------------------------------------------------"
echo "Build program 1..."
clang++ program1.cpp -o program1exe
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe "
echo ""
echo "EXPECTED OUTPUT: Table of variables,"
echo "Step 1, 2, 3, and 4, displaying ptrInt pointing to",
echo "and value of item at that address (via pointer)..."
echo ""
echo "ACTUAL OUTPUT:"
./program1exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program1.cpp


echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 2..."
clang++ program2.cpp -o program2exe
echo "-------------------------------------------------"
echo "TEST 1: ./program2exe"
echo ""
echo "EXPECTED OUTPUT: Table of string variables,"
echo "Step 1, 2, 3, and 4 to update student names (vai pointer)..."
echo ""
echo "ACTUAL OUTPUT:"
./program2exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program2.cpp


echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 3..."
clang++ program3.cpp -o program3exe
echo "-------------------------------------------------"
echo "TEST 1: ./program3exe"
echo ""
echo "EXPECTED OUTPUT:"
echo "Student info (BEFORE UPDATE):"
echo "* Name: UNASSIGNED"
echo "* GPA: 0.0"
echo ""
echo "Student info (AFTER UPDATE):"
echo "* Name: (some value)"
echo "* GPA: (some value)"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program3.cpp



echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 4..."
clang++ program4.cpp -o program4exe
echo "-------------------------------------------------"
echo "TEST 1: ./program4exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program4exe input_file"
echo ""
echo "ACTUAL OUTPUT:"
./program4exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program4exe invalid_file.txt"
echo ""
echo "EXPECTED OUTPUT: COULD NOT OPEN FILE"
echo ""
echo "ACTUAL OUTPUT:"
./program4exe invalid_file.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program4exe p4_data1.txt"
echo ""
echo "EXPECTED OUTPUT: Displays contents of p4_data1.txt..."
cat p4_data1.txt
echo ""
echo "ACTUAL OUTPUT:"
./program4exe p4_data1.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program4exe p4_data2.txt"
echo ""
echo "EXPECTED OUTPUT: Displays contents of p4_data2.txt..."
cat p4_data2.txt
echo ""
echo "ACTUAL OUTPUT:"
./program4exe p4_data2.txt
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program4.cpp



echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 5..."
clang++ program5.cpp -o program5exe
echo "-------------------------------------------------"
echo "TEST 1: ./program5exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program5exe input_file"
echo ""
echo "ACTUAL OUTPUT:"
./program5exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program5exe p5_bank1.txt"
echo ""
echo "p5_bank1.txt before:"
cat p5_bank1.txt
echo ""
echo "Program should remove 50 from account 0..."
echo ""
echo "p5_bank1.txt after:"
cat p5_bank1.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program5exe p5_bank2.txt"
echo ""
echo "p5_bank2.txt before:"
cat p5_bank2.txt
echo ""
echo "Program should remove 50 from account 0..."
echo ""
echo "p5_bank2.txt after:"
cat p5_bank2.txt

echo ""
echo "-------------------------------------------------"
echo "TEST CASES:"
cat program5_tests.txt
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program5.cpp


echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 6..."
clang++ program6.cpp -o program6exe
echo "-------------------------------------------------"
echo "TEST 1: ./program6exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program6exe input_file account_index action amount"
echo ""
echo "ACTUAL OUTPUT:"
./program6exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program6exe p5_bank1.txt -5 W 9.98"
echo ""
echo "EXPECTED OUTPUT: INVALID INDEX ERROR"
echo ""
echo "ACTUAL OUTPUT:"
./program6exe p5_bank1.txt -5 W 9.98

echo ""
echo "-------------------------------------------------"
echo "TEST 3: ./program6exe p5_bank1.txt 100 W 9.98"
echo ""
echo "EXPECTED OUTPUT: INVALID INDEX ERROR"
echo ""
echo "ACTUAL OUTPUT:"
./program6exe p5_bank1.txt 100 W 9.98

echo ""
echo "-------------------------------------------------"
echo "TEST 4: ./program6exe p5_bank1.txt 1 Z 9.98"
echo ""
echo "EXPECTED OUTPUT: INVALID TRANSACTION CODE ERROR"
echo ""
echo "ACTUAL OUTPUT:"
./program6exe p5_bank1.txt 1 Z 9.98

echo ""
echo "-------------------------------------------------"
echo "TEST 5: ./program6exe p5_bank1.txt 1 W 100000"
echo ""
echo "EXPECTED OUTPUT: CANNOT WITHDDRAW MORE THAN BALANCE ERROR"
echo ""
echo "ACTUAL OUTPUT:"
./program6exe p5_bank1.txt 1 W 100000

echo ""
echo "-------------------------------------------------"
echo "TEST 6: ./program6exe p5_bank2.txt 2 W 100"
echo ""
echo "p5_bank2.txt before:"
cat p5_bank2.txt
echo ""
echo "Withdraw 100 from account 2:"
./program6exe p5_bank2.txt 2 W 100
echo ""
echo "p5_bank2.txt after:"
cat p5_bank2.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 7: ./program6exe p5_bank1.txt 2 D 50"
echo ""
echo "p5_bank1.txt before:"
cat p5_bank1.txt
echo ""
echo "Deposit 50 to account 1:"
./program6exe p5_bank1.txt 1 D 50
echo ""
echo "p5_bank1.txt after:"
cat p5_bank1.txt

echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
cat program6.cpp
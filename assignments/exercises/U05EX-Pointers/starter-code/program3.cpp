#include <iomanip>  /* Use setprecision */
#include <iostream> /* Use cout */
#include <string>   /* Use strings */
using namespace std;

struct Student {
  string name;
  float gpa;
};

int main() {
  Student newStudent;
  newStudent.name = "UNASSIGNED";
  newStudent.gpa = 0;

  cout << fixed << setprecision(1);
  cout << "Student info (BEFORE UPDATE):" << endl;
  cout << "* Name: " << newStudent.name << endl;
  cout << "* GPA:  " << newStudent.gpa << endl;
  cout << endl;

  // Add code here

  cout << "Student info (AFTER UPDATE):" << endl;
  cout << "* Name: " << newStudent.name << endl;
  cout << "* GPA:  " << newStudent.gpa << endl;
  cout << endl;

  /* Quit program with code 0 (no errors) */
  return 0;
}
#include <iostream>
#include <string>
using namespace std;

#include "Player.h"
#include "PlayerFunctions.h"
#include "ProgramFunctions.h"

int main() {
  cout << "CHARACTER PROGRAM" << endl;

  // TODO: Create a Player object here

  string name;
  int gold;
  int level;

  bool done = false;
  while (!done) {

    // TODO: Display character info here

    DisplayMainMenu();
    char choice = GetCharChoice();
    cout << "You entered: " << choice << endl;

    switch (choice) {
    case 'I':
      // TODO: Init player
      break;

    case 'E':
      // TODO: Edit player
      break;

    case 'L':
      // TODO: Load player
      break;

    case 'S':
      // TODO: Save player
      break;

    case 'X':
      done = true;
      break;

    default:
      cout << "UNKNOWN COMMAND" << endl;
    }
  }

  cout << endl << "GOODBYE!" << endl;
  return 0;
}
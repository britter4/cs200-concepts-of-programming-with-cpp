clear
echo "-------------------------------------------------"
echo "Build program 1..."
g++ program1/*.h program1/*.cpp -o program1exe
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe "
echo ""
echo "EXPECTED OUTPUT: Call DoSomething1(), DoSomething2(), then quit"
echo ""
echo "ACTUAL OUTPUT:"
echo 1 2 3 | ./program1exe
echo ""
echo "-------------------------------------------------"
echo "CODE:"
cat program1/functions.cpp


echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 2..."
cd program2
g++ *.h *.cpp -o program2exe
echo "-------------------------------------------------"
echo "TEST 1: ./program2exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program2exe input_file"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe
echo "-------------------------------------------------"
echo "TEST 2: ./program3exe item_prices.txt"
echo ""
echo "EXPECTED OUTPUT: Display contents of file"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe
echo ""
echo "-------------------------------------------------"
echo "CODE:"
echo "ArrayTools.cpp:"
cat ArrayTools.cpp
echo "program2.cpp:"
cat program2.cpp

cd ..

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 3..."
cd program3
g++ *.h *.cpp -o program3exe
echo "-------------------------------------------------"
echo "TEST 1: ./program3exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program3exe input_file"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe
echo "-------------------------------------------------"
echo "TEST 2: ./program3exe sum - inputs 2 and 3"
echo ""
echo "EXPECTED OUTPUT: Sum: 5"
echo ""
echo "ACTUAL OUTPUT:"
echo 2 3 | ./program3exe sum
echo ""
echo "-------------------------------------------------"
echo "TEST 3: ./program3exe area - inputs 2 and 3"
echo ""
echo "EXPECTED OUTPUT: Area: 6"
echo ""
echo "ACTUAL OUTPUT:"
echo 2 3 | ./program3exe area
echo ""
echo "-------------------------------------------------"
echo "TEST 4: ./program3exe slope - inputs 1 3 2 6"
echo ""
echo "EXPECTED OUTPUT: Slope: 3"
echo ""
echo "ACTUAL OUTPUT:"
echo 1 3 2 6 | ./program3exe slope
echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program3exe tests"
echo ""
echo "EXPECTED OUTPUT: Tests"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe tests
echo ""
echo "-------------------------------------------------"
echo "CODE:"
echo "ArrayTools.cpp:"
cat Math.cpp
echo "MathTester.cpp:"
cat MathTester.cpp
echo "program3.cpp:"
cat program3.cpp

cd ..

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 4..."
cd program4
g++ *.h *.cpp -o program4exe
echo "-------------------------------------------------"
echo "TEST 1: ./program4exe - inputs I S X"
echo ""
echo "EXPECTED OUTPUT: Character is initialized and saved"
echo ""
echo "ACTUAL OUTPUT:"
echo "I S X" | ./program4exe
echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program4exe - inputs L X"
echo ""
echo "EXPECTED OUTPUT: Character is loaded"
echo ""
echo "ACTUAL OUTPUT:"
echo "L X" | ./program4exe
echo ""
echo "-------------------------------------------------"
echo "TEST 3: ./program4exe - inputs E X"
echo ""
echo "EXPECTED OUTPUT: Character is edited"
echo ""
echo "ACTUAL OUTPUT:"
echo "E Luna 1 2 X" | ./program4exe
echo ""
echo "-------------------------------------------------"
echo "CODE:"
echo "PlayerFunctions.cpp:"
cat PlayerFunctions.cpp
echo "program4.cpp:"
cat program4.cpp

cd ..

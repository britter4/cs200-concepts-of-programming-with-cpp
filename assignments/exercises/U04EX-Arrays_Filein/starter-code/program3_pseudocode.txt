Check to make sure there are enough arguments
  - If there aren't enough arguments, display an example program run, and exit with an error code.

Store argument #1 into a string variable named filename.

Create a float variable named balance.

Create an input file stream object and try to open the filename given. (If it fails, display an error message and exit with an error code.)

Load the data from the input file to the balance variable.

Close the input file.

Display the current balance to the screen.

Ask the user how much they'd like to deposit, and store it in a new float variable named deposit. (use cin)

If the deposit amount is valid (greater than 0), then add the deposit amount to the balance.

Display the updated balance.

Ask the user how much they'd like to withdraw, and store it in a new float variable named withdraw. (use cin)

If the withdraw amount is valid (greater than 0, less than or equal to the balance) then subtract it from the balance.

Display the updated balance.

Create an output file stream varaible and use the same filename as before to open.

Output the balance variable to the text file via your ofstream.

Display a message like "Updated balance saved to bank.txt".
clear
echo "-------------------------------------------------"
echo "Build program 1..."
clang++ program1.cpp -o program1exe
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe "
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program1exe input_file"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program1exe numbers1.txt"
echo ""
echo "EXPECTED OUTPUT: 5 + 60 + 99 + 10 = 26"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe numbers1.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program1exe numbers2.txt"
echo ""
echo "EXPECTED OUTPUT: 5 + 4 + 3 + 2 = 14"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe numbers2.txt

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 2..."
clang++ program2.cpp -o program2exe
echo "-------------------------------------------------"
echo "TEST 1: ./program2exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program2exe input_file"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program2exe grades1.txt"
echo ""
echo "EXPECTED OUTPUT:"
echo "COURSE     GRADE"
echo "---------------"
echo "CS134      4.0"
echo "CS200      3.5"
echo "CS210      4.0"
echo "ASL120     3.0"
echo ""
echo "GPA: 3.625"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe grades1.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program2exe grades2.txt"
echo ""
echo "EXPECTED OUTPUT:"
echo "COURSE     GRADE"
echo "---------------"
echo "CS210      2.0"
echo "CS211      2.5"
echo "CS200      3.0"
echo "CS235      3.5"
echo ""
echo "GPA: 2.75"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe grades2.txt

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 3..."
clang++ program3.cpp -o program3exe
echo "-------------------------------------------------"
echo "TEST 1: ./program3exe"
echo ""
echo "EXPECTED OUTPUT: NOT ENOUGH ARGUMENTS"
echo "Expected form: program4exe input_file"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe

echo ""
echo "-------------------------------------------------"
echo "TEST 2: ./program3exe bank.txt"
echo ""
echo "bank.txt before:"
cat bank.txt
echo ""
echo "Deposit 50 and withdraw 25:"
echo 50 25 | ./program3exe bank.txt
echo ""
echo "bank.txt after:"
cat bank.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 3: Test invalid deposit"
echo ""
echo "bank.txt before:"
cat bank.txt
echo ""
echo "Deposit -9 and withdraw 25:"
echo -9 25 | ./program3exe bank.txt
echo ""
echo "bank.txt after:"
cat bank.txt

echo ""
echo "-------------------------------------------------"
echo "TEST 4: Test invalid withdraw"
echo ""
echo "bank.txt before:"
cat bank.txt
echo ""
echo "Deposit 20 and withdraw 10000:"
echo 20 10000 | ./program3exe bank.txt
echo ""
echo "bank.txt after:"
cat bank.txt

echo ""
echo "-------------------------------------------------"
echo "TEST CASES:"
cat program3_tests.txt

#include <iostream>
#include <vector>
using namespace std;

void Display_Iter(vector<string> list) {
}

void Display_Rec(vector<string> list, int index) {
}

int main() {
  vector<string> list1 = {"CS134", "CS200", "CS235"};
  vector<string> list2 = {"Kabe", "Luna", "Pixel", "Korra"};

  cout << endl << "ITERATIVE VERSIONS:" << endl;
  cout << "List 1: " << endl;
  Display_Iter(list1);
  cout << endl << "List 2: " << endl;
  Display_Iter(list2);

  cout << endl << "RECURSIVE VERSIONS:" << endl;
  cout << "List 1: " << endl;
  Display_Rec(list1, 0);
  cout << endl << "List 2: " << endl;
  Display_Rec(list2, 0);

  /* Quit program with code 0 (no errors) */
  cout << endl << "GOODBYE" << endl;
  return 0;
}

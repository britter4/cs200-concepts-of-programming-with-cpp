#include <iostream> /* Use cout */
using namespace std;

void CountUp_Iter(int start, int end) {
}

void CountUp_Rec(int start, int end) {
}

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 3) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " start end" << endl;
    return 1; /* Exit with error code 1 */
  }

  int start = stoi(argumentList[1]);
  int end = stoi(argumentList[2]);

  cout << endl << endl << "ITERATIVE VERSION:" << endl;
  CountUp_Iter(start, end);

  cout << endl << endl << "RECURSIVE VERSION:" << endl;
  CountUp_Rec(start, end);

  /* Quit program with code 0 (no errors) */
  cout << endl << endl << "GOODBYE" << endl;
  return 0;
}
